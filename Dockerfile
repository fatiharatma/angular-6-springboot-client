# base image
FROM node:10.16.3 as build

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

# add app
COPY . /app

# start app
#CMD ng serve --host 0.0.0.0

# generate build
RUN ng build --output-path=dist

#ngnix
FROM nginx:1.16.0-alpine

#copy artifact build from dist to ngnix
COPY --from=build /app/dist /usr/share/nginx/html

EXPOSE 80

#run ngnx
CMD ["nginx", "-g", "daemon off;"]
