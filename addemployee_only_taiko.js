const { openBrowser, goto, write, click } = require('taiko');
(async () => {
  try {
    await openBrowser();
    await goto("http://localhost/");
    await click("Add");
  } catch (error) {
    console.error(error);
  } finally {
    closeBrowser();
  }
})();
