# Angular6SpringbootClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


#Local Development
rm -rf node_modules/
rm -rd dist/
npm install
sudo npm install -g @angular/cli@7.3.9
ng serve 

#docker 
docker build -t training-angular-springboot-client:latest .

# docker run for ng (DockerfileDev : Development ortamı için nginx olmadan)
docker run -v ${PWD}:/app -v /app/node_modules -p 4201:4200 --rm training-angular-springboot:latest

# docker run for nginx 
docker build -t training-angular-springboot-client:latest .

docker run -p 80:80 -d training-angular-springboot-client:latest

# docker push gitlab
docker login registry.gitlab.com -u fatiharatma
docker build -t registry.gitlab.com/fatiharatma/angular-6-springboot-client .
docker push registry.gitlab.com/fatiharatma/angular-6-springboot-client


#minikube
brew cask install minikube
minikube start --vm-driver hyperkit
minikube dashboard

-- Deployment Oluşturmak için yaml file oluştur.


        -- Create the backend Deployment:
                kubectl apply -f angularClientDeployment.yaml
                
        -- gitlab repo public yapılmalı veya özel oalrak private key üretilip deploy edilmeli.                
        
        -- Deployment detayını gör
            kubectl describe deployment angularclient
     
        -- Deploymentları gör  
                  kubectl get deployments
        
        -- Podları gör          
                  kubectl get pods
                  
        -- View cluster events:
                  kubectl get events
        
        -- View the kubectl configuration:
                  kubectl config view
         
         -- POD da shell çalıştırmak
         kubectl exec -it angularclient-6d6b8d5f54-74hxt  -- /bin/bash
         OR
         kubectl exec -it angularclient-6d6b8d5f54-74hxt  -- /bin/sh
-- Service Oluştur
        -- Service için yaml file oluştur.
        
        -- Service oluştur.
            kubectl apply -f angularClientKubernetesServices.yaml
        
        -- Servicleri gör
             kubectl get services
        
        -- Service detayını gör IP vb ;
         
            kubectl get services angularclient --watch
              
         -- Podları gör
              kubectl get pods 
          
          -- Sadece bir poda yönlendirmek için ; Port forward işlemi
          kubectl port-forward  %POD NAME% 8080:80
          
          kubectl port-forward  angularclient-7f7695ff89-gq5f5  8080:80

          -- Yönlendirme yapıldıktan sonra  uygulamayı görebiliriz.
          
          http://localhost:8080/
          
        -- Birden fazla pod oluşturup, belirli bir pod yerine loadbalancer şeklinde yönlendirmme yapmak için service yaml dosyasında;
          type: NodePort olarak services da set edilmeli ve nodePort: 30003 belirtilmeli.
        
                -- minikube ip gör
                    minikube ip
                 
                -- Minikube üzerinden ulaş
                    http://192.168.64.2:30003/
                    
        -- Minikube stop
            minikube stop
            
             kubectl get pods ile kontrol edilebilir.
        
  exechary
  
-- Clean
        kubectl delete service angularclient
        kubectl delete deployment angularclient

#kubernetes
        - Kubernete Konfigusaryon Dosyası : /Users/fatiharatma/.kube/config
        - current-context de aktif olan kubernetes cluster ını gösterir. burdaki değer ne ise kubectls komutu o ortamda 
        çalışır.
        Ör: minikube start dediğimizde bu current context değişir.  
            veya gcloud config set project inner-line-255007
        ile gcloud konfigurasyonu yapılabilir.
        
#Gloud
    Login  :  gcloud auth login
    Proje Listesi görebilmek için : gcloud projects list
    Proje konfigurasyonu : gcloud config set project inner-line-255007
    Cluster Set:  gcloud container clusters get-credentials training-cluster --zone us-central1-a --project inner-line-255007
        -- Create the backend Deployment:
                kubectl apply -f angularClientDeployment.yaml
        
        -- Service oluştur. Bu serviste type olarak LoadBalancer seçilmeli.
                 kubectl apply -f angularClientKubernetesServicesForLoadBalancerType.yaml
            
        -- ip bilgisi gör : kubectl get services angularclient
            
        -- Gcloud üzerinden ulaş
            http://104.154.162.19:80/

        
# GitLabdan Gloud kubernetes deploy
    -- Yetki Ayarları : 
        -- Web conseolde IAM - Service Accunts - Create Service accounts -  yeni hesap oluştur.
         project rolü seçip JSON file download edilir.
    
    -- GCloudda Logout ol: gcloud auth revoke
    -- Json key ile login ol : gcloud auth activate-service-account --key-file loginGCloudProjectTraining.json
    
    -- Json file encode edilir.
        cat loginGCloudProjectTraining.json| base64 > encoded_loginGCloudProjectTraining.json
   
    -- Gitlab CI/CDI ayarları yapılarak GCloudda işlem yapılabilmesi için environment tanımlanır.
          Gitlab -  Settings - Variables  bölümünde YUKARDA üretilen BASE64 scripti tanım yapılır.
          
          SERVICE_ACCOUNT_KEY ve değeri komutla üretilen text.
          
    -- gitlab-ci.yml da gcloud deploy için scriptler yazılır.
    
# gauge kurulumu 
    -  brew install gauge
    or
     - npm install gauge

    - npm install @getgauge/cli
    - gauge install js

# Taiko kurulumu
    -  sudo npm install -g taiko --unsafe-perm --allow-rootCopy
        

# Local ortamda gauge , taiko ile Acceptance testlerinin koşulması
    -- http://uidevdaily.com/2019/test-automation-with-gauge-and-taiko/
    -- https://docs.gauge.org/writing-specifications.html?os=macos&language=javascript&ide=vscode
    -- https://taiko.gauge.org
    
    # Projeye gauge ve taiko ozelligi eklemek için
        gauge init js
    
    # Bu scripte package.json dosyasını bozuyor, bu dosyayı eski haline alıp,
        "test": "gauge run specs/"
            ve
         "taiko": "latest"
           bölümlerini manuel olarak eklemek gerekli.
            
    # Run TEST
        gauge run specs
        Bu komut  specs altındaki tüm spec dosyaları, tests altındaki js dosyaları ile çalıştırılmasını sağlar.
        
     # Gauge ile spec senaryo dosyası yazmadan  taiko ile çalıştırmak için 
        taiko addemployee_only_taiko.js --observe
         
# GCloud ortamda gauge , taiko ile Acceptance testlerinin koşulması
    - Prod deployu öncesi, acceptance testlerinin koşulması için test deployment step eklenmeli ve gcloud a test uygulaması deploy edilmeli.
        
    - Aynı gcloud cluster a, aynı imaj farklı bir servis olarak TEST ortamı amacıyla kullanılacak şekilde deploy edilmeli.
    - Bunun için test ve pord depmoyment / service yaml dosyaları ayrıldı.  
         # PROD   
            - kubectl apply -f ENV_PROD_angularClientKubernetesDeployment.yaml
            - kubectl apply -f ENV_PROD_angularClientKubernetesServicesForLoadBalancerType.yaml
         # TEST
            - kubectl apply -f ENV_TEST_angularClientKubernetesDeployment.yaml
            - kubectl apply -f ENV_TEST_angularClientKubernetesServicesForLoadBalancerType.yaml
    
    - env klasörü de commit edilmeli, gauge taiko konfigleri oldugu icin.
    - docker da calistigi icin sandbox pasif şekilde gauge run çalıştırılmalı.
    

    - Testlerin koşulacağu ip:port bilgisi bir şekilde acceptace test scriptlerine geçilmeli. Çünkü servis IP bilgisi değişebilir deploylarda. Deploy sonrası
    dinamik olarak test ortam deployu alınabilir.
           
        # Service IP bilgisini görme   
            kubectl get svc test-angularclient --output jsonpath='{.status.loadBalancer.ingress[0].ip}'
    
        # Servis IP bilgisini kullanmak icin text dosyaya yazma
        
             # Test ip bilgisini bir variable set et.
                echo "export TEST_CLUSTER_IP=`kubectl get svc test-angularclient --output jsonpath='{.status.loadBalancer.ingress[0].ip}'`" > test_cluster_variable
        
             # test scriptinin yer aldığı js dosyasının içereisnde <TEST_IP> olarak belirtilen bölümü değişkenin değeriyle replace et.
                sed -i "s/<TEST_IP>/${TEST_CLUSTER_IP}/g" tests/addemployee_step_implementation.js


    
    
        




