/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press,text, contains, click } = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
  await openBrowser({args: [
      '--disable-gpu',
      '--disable-dev-shm-usage',
      '--disable-setuid-sandbox',
      '--no-first-run',
      '--no-sandbox',
      '--no-zygote']})

  /*For open chrome to screen*/
  /*await openBrowser({ headless: headless })*/
});

afterSuite(async () => {
  await closeBrowser();
});

step("Goto Main Page", async () => {
  await goto("http://<TEST_IP>:90");
});

step("Click Add Button <query>", async (query) => {
  await click(query)
});

step("Page contains <content>", async (content) => {
  assert.ok(await text(content).exists());
});
